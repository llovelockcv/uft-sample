# LoginFlights for UFT agent sample

An sample script stimulates scenario:

    1- Open flight application
    2- Login with account usernname: John and password: hp
    3- Search a flight
    4- Quit the flight application

PREREQUISITES:

    - Install UFT in machine
    - Download this source code and unzip it into C drive as C:\uft-sample\
    - Open UFT tool in your machine and run this sample to make sure it run successfully.

# TCM automation host
 This section will introduce how to setup a TCM Automation Integration with UFT agent

# How to use sample project to integrate with TCM Automation Host
  1. First, download and install TCM Automation Host
  2. Save the UFT sample at C:\uft-sample
  3. Configure host as per TCM documentation
